<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Notification;

class FirstController extends Controller
{

    public function index()
    {
		$users = DB::select('select * from users');
		return view('home',['users'=>$users]);
	}

	public function insert(Request $request)
    {
    	$textfield = $request->input('textfield');

    	$data=array('textfield'=>$textfield);

		DB::table('test1')->insert($data);
		echo "Notified";
	}

	public function insertform()
	{
		return view('/home');
	}
}