<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use App\Message;

class HomeController extends Controller
{
    
    public function __construct()
    {
        $this->middleware('auth');
    }
    
   
    public function index()
    {
        $following = Auth::user()->following->pluck('id');

        $messages = Message::whereIn('user_id',$following)->orWhere('user_id',Auth::user()->id)->get();

        return view('home',[
            'messages'=>$messages
        ]);
    }
}
