<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;

class TaskCompleted extends Notification
{
    use Queueable;

    public $user;

    public function __construct()
    {
         // $this->user=$user;
    }

    public function via($notifiable)
    {
        return ['database'];
    }

    public function toArray($notifiable)
    {
        return [
            'data' => 'Admin added a new post'
        ];
    }

    public function toDatabase($notifiable)
    {
        return [
            // 'id'=>$this->user->id,
            // 'name'=>$this->user->name
        ];
    }
}