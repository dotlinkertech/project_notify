<?php

use Illuminate\Support\Facades\Route;
use App\User;
use App\Notifications\TaskCompleted;

Route::get('/', function () {
    return view('welcome');
});

Route::get('markAsRead',function(){
    auth()->user()->unreadNotifications->markAsRead();
    return redirect()->back();
})->name('markRead');

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::post('/follow', 'ProfileController@followOrUnfollowUser');

Route::get('/u/{user}','ProfileController@index');

Route::get('insert','FirstController@insertform');
Route::post('create','FirstController@insert');

Route::get('/home','FirstController@index');

Route::get('/x',function(){
	foreach (Auth::user()->notifications as $notification) {
		
	}
});